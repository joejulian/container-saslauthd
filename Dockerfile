FROM golang:1.12 as fsyslog
RUN go get gitlab.com/joejulian/fsyslog

FROM registry.gitlab.com/joejulian/docker-arch:latest

RUN pacman -Syu --noconfirm cyrus-sasl && pacman -Scc --noconfirm && rm -rf /var/cache/pacman/pkg
COPY --from=fsyslog /go/bin/fsyslog /bin/fsyslog

CMD fsyslog -command "saslauthd -d -a ldap -O /config/saslauthd.conf -m /socket"
