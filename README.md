# saslauthd container

The default configuration for this container is to authenticate to an ldap 
server. 
This container needs to run in the same ipc namespace as the container which is
utilizing this service and with a shared directory.
The intended use of this container is in Kubernetes.

## Usage

A sample pod using saslauthd
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postfix-deployment
  labels:
    app: postfix
spec:
  replicas: 1
  selector: 
    matchLabels
      app: postfix
  template:
    metadata:
      labels:
        app: postfix
    spec:
      containers:
        - name: saslauthd
          image: registry.gitlab.com/joejulian/container-saslauthd:latest
          imagePullPolicy: Always
          volumeMounts:
            - name: sasl_config
              mountPath: /config
            - name: socket
              mountPath: /socket
        - name: postfix
          image: registry.gitlab.com/joejulian/container-saslauthd:latest
          imagePullPolicy: Always
          volumeMounts:
            - name: postfix_config
              mountPath: /config
            - name: socket
              mountPath: /socket
      volumes:
        - name: sasl_config
          configMap:
            name: saslauthd
        - name: postfix_config
          configMap:
            name: postfix
        - name: socket
          emptyDir: {}
```

NOTE: Notice the shared `socket` emptydir. This is where the unix socket will
be created by saslauthd and consumed by postfix.
